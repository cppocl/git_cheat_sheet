# Git Cheat Sheet

## Clone (copy) the source
```
git clone <url>
```

Example:
```
git clone https://gitlab.com/cppocl/git_cheat_sheet
```

## Remove all local changes
NOTE: Check there is no stash before using the command.
```
git reset --hard HEAD
```

## Update local branch to latest
```
git pull
```

## Update local branch to latest with local changes, which will also perform a merge.
```
git pull --rebase
```

## Checkout a specific tagged version
```
git checkout <tagname>
```

## Create a local branch
```
git checkout -b <branch>
```

Create a branch from a specific tagged version:
```
git checkout -b <branch> <tag>
```

or

```
git checkout tags/<tag> -b <branch>
```

## Move local changes from unstaged to staged
```
git add .
```

## Commit local changes (which includes untaged files)
```
git commit -a -m "This ms my message"
```

Commit all changes including added or removed files:
```
git commit -A -m "This ms my message"
```

## Upload local changes to remote
```
git push
```

Upload with a message:
```
git push -m "This is my message"
```

## Tag a local version and upload tag to remote
```
git tag <tagname>
git push origin --tags
```

Tag a version with a description:
```
git tag -a <tagname> -m "This is my message"
```

## Show change log
```
git log
```

Show a log within a date range
```
git log --after=<start> --until=<end>
```

Example of date range log:
```
git log --after="2021-01-31" --until="2023-03-31"
```

## List users for a project
```
git shortlog --summary --numbered --email
```

## List commits for an author
```
git log --author="username" -p
```
